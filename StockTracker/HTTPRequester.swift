//
//  HTTPRequester.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

protocol HTTPRequestable {
    func performRequest(scheme: String, host: String, path: String, queryItems: [URLQueryItem]?, completion: @escaping (Result<Data>) -> Void)
}

class HTTPRequester: HTTPRequestable {
    
    static let instance = HTTPRequester()
    
    func performRequest(scheme: String, host: String, path: String, queryItems: [URLQueryItem]? = nil, completion: @escaping (Result<Data>) -> Void) {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = queryItems
        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            DispatchQueue.main.async {
                if let error = responseError {
                    completion(.failure(error))
                } else if let jsonData = responseData {
                    completion(.success(jsonData))
                } else {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                    completion(.failure(error))
                }
            }
        }
        
        task.resume()
    }
}
