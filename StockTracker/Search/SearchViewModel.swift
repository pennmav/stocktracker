//
//  SearchViewModel.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Observable

protocol SearchViewModelable {
    var searchResults: Observable<[StockData]?> { get }
    func retrieveStockData(for symbol: String)
    func clearSearchResults()
}

class SearchViewModel: SearchViewModelable {
    var searchResults: Observable<[StockData]?> = Observable(nil)
    private let realTimeStockService: RealTimeStockDataRetriever
    
    init(realTimeStockService: RealTimeStockDataRetriever = RealTimeStockService()) {
        self.realTimeStockService = realTimeStockService
    }
    
    func retrieveStockData(for symbol: String) {
        realTimeStockService.getStockData(for: symbol) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let stockData):
                self.searchResults.value = stockData
            case .failure(let error):
                self.searchResults.value = []
                print(error.localizedDescription)
            }
        }
    }
    
    func clearSearchResults() {
        guard searchResults.value != nil else { return }
        searchResults.value = []
    }
}
