//
//  SearchViewController.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/16/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Observable
import UIKit

private enum Section: Int, CaseIterable {
    case symbols = 0
}

protocol Searchable: AnyObject {
    func focusSearch()
}

class SearchViewController: UIViewController {
    private let searchBar = UISearchBar().withDone
    private let tableView = UITableView()
    private let viewModel: SearchViewModelable
    
    private var disposal = Disposal()
    
    init(viewModel: SearchViewModelable = SearchViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupSearchBar()
        setupTableView()
        bindToViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarController?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Hint", style: .plain, target: self, action: #selector(showSearchHint))
        tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clearSearchResults))
        tabBarController?.navigationItem.titleView = searchBar
        searchBar.sizeToFit()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.navigationItem.leftBarButtonItem = nil
        tabBarController?.navigationItem.titleView = nil
        tabBarController?.navigationItem.rightBarButtonItem = nil
    }
    
    private func bindToViewModel() {
        viewModel.searchResults.observe { [weak self] newValue, _ in
            guard let self = self, newValue != nil else { return }
            self.tableView.reloadSections(IndexSet(integer: Section.symbols.rawValue), with: .automatic)
        }.add(to: &disposal)
    }
    
    private func setupSearchBar() {
        searchBar.delegate = self
        searchBar.placeholder = "Search"
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.tableFooterView = UIView()
        
        tableView.register(StockCell.self, forCellReuseIdentifier: "\(StockCell.self)")
    }

    @objc func showSearchHint() {
        let hint = UIAlertController(title: "Hint", message: "Did you know you can search for more than one stock at a time? Try entering \"AAPL,GOOG,MSFT\" to give it a try!", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        hint.addAction(dismissAction)
        present(hint, animated: true, completion: nil)
    }
    
    @objc func clearSearchResults() {
        viewModel.clearSearchResults()
        searchBar.text = ""
    }
}

extension SearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchResults.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section),
            let searchResults = viewModel.searchResults.value, searchResults.count > indexPath.row else { return UITableViewCell() }
        switch section {
        case .symbols:
            let stockData = searchResults[indexPath.row]
            let stockCell = tableView.dequeueReusableCell(withIdentifier: "\(StockCell.self)") as! StockCell
            stockCell.viewModel = StockCellViewModel(stockData: stockData)
            return stockCell
        }
    }
}

extension SearchViewController: UITableViewDelegate {
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text, searchText.count > 0 else { return }
        searchBar.resignFirstResponder()
        viewModel.retrieveStockData(for: searchText)
    }
}

extension SearchViewController: Searchable {
    func focusSearch() {
        searchBar.becomeFirstResponder()
    }
}
