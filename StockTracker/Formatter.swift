//
//  Formatter.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

class Formatter {
    private static var currency: NumberFormatter {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "en_US")
        return currencyFormatter
    }
    
    static func currency(from string: String) -> String? {
        guard let value = Decimal(string: string) else { return nil }
        return Formatter.currency.string(for: value)
    }
}
