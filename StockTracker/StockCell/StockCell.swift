//
//  StockCell.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import UIKit
import Observable

typealias StockTableViewCell = StockCellConfigurable & UITableViewCell

protocol StockCellConfigurable {
    var viewModel: StockCellViewModelable? { get set }
    var updatesDelegate: UITableViewUpdatesDelegate? { get set }
}

class StockCell: UITableViewCell, StockCellConfigurable {
    private let symbolLabel = UILabel()
    private let companyNameLabel = UILabel()
    private let priceLabel = UILabel()
    private let percentChangeLabel = UILabel()
    private let addButton = UIButton(type: UIButton.ButtonType.contactAdd)
    
    private var disposal = Disposal()
    private var priceKey: String?
    
    weak var updatesDelegate: UITableViewUpdatesDelegate?
    
    var viewModel: StockCellViewModelable? {
        didSet {
            guard let viewModel = viewModel else { return }
            setupSaveButton()
            
            viewModel.symbol.observe { [weak self] newValue, _ in
                guard let self = self, let newValue = newValue else { return }
                self.priceKey = newValue
                self.updatesDelegate?.beginUpdates()
                self.symbolLabel.text = newValue
                self.updatesDelegate?.endUpdates()
            }.add(to: &disposal)
            
            viewModel.companyName.observe { [weak self] newValue, _ in
                guard let self = self, let newValue = newValue else { return }
                self.updatesDelegate?.beginUpdates()
                self.companyNameLabel.text = newValue
                self.updatesDelegate?.endUpdates()
            }.add(to: &disposal)
            
            viewModel.price.observe { [weak self] newValue, _ in
                guard let self = self, let newValue = newValue, newValue.key == self.priceKey else { return }
                self.updatesDelegate?.beginUpdates()
                self.priceLabel.text = Formatter.currency(from: newValue.price)
                self.updatesDelegate?.endUpdates()
            }.add(to: &disposal)
            
            viewModel.percentChange.observe { [weak self] newValue, _ in
                guard let self = self, let newValue = newValue else { return }
                self.updatesDelegate?.beginUpdates()
                self.percentChangeLabel.text = newValue + "%"
                self.updatesDelegate?.endUpdates()
            }.add(to: &disposal)
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSymbolLabel()
        setupCompanyNameLabel()
        setupPriceLabel()
        setupPercentChangeLabel()
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupSymbolLabel() {
        contentView.addSubview(symbolLabel)
        symbolLabel.translatesAutoresizingMaskIntoConstraints = false
        symbolLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24).isActive = true
        symbolLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
    }
    
    private func setupCompanyNameLabel() {
        contentView.addSubview(companyNameLabel)
        companyNameLabel.translatesAutoresizingMaskIntoConstraints = false
        companyNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24).isActive = true
        companyNameLabel.topAnchor.constraint(equalTo: symbolLabel.bottomAnchor, constant: 8).isActive = true
        let constraint = companyNameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        constraint.priority = .defaultLow
        constraint.isActive = true
    }
    
    private func setupPriceLabel() {
        contentView.addSubview(priceLabel)
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24).isActive = true
        priceLabel.lastBaselineAnchor.constraint(equalTo: symbolLabel.lastBaselineAnchor).isActive = true
    }
    
    private func setupPercentChangeLabel() {
        contentView.addSubview(percentChangeLabel)
        percentChangeLabel.translatesAutoresizingMaskIntoConstraints = false
        percentChangeLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24).isActive = true
        percentChangeLabel.lastBaselineAnchor.constraint(equalTo: companyNameLabel.lastBaselineAnchor).isActive = true
    }
    
    private func setupSaveButton() {
        guard viewModel?.saveable == true else { return }
        addButton.addTarget(self, action: #selector(addToFavorites), for: .touchUpInside)
        accessoryView = addButton
    }
    
    @objc private func addToFavorites() {
        accessoryView = nil
        viewModel?.favoriteStock()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.priceKey = nil
    }
}
