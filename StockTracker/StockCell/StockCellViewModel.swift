//
//  StockCellViewModel.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation
import Observable

typealias PriceWithKey = (key: String, price: String)
typealias FavoritedStockCellViewModel = StockCellViewModelable & StockCellViewModelUpdateable

protocol StockCellViewModelable {
    var symbol: Observable<String?> { get }
    var companyName: Observable<String?> { get }
    var price: Observable<PriceWithKey?> { get }
    var percentChange: Observable<String?> { get }
    var saveable: Bool { get }
    func favoriteStock()
}

protocol StockCellViewModelUpdateable {
    func stopUpdates()
}

class StockCellViewModel: StockCellViewModelable {
    private let favoritesService: FavoritesService?
    private var timer: Timer?
    
    var symbol: Observable<String?> = Observable(nil)
    var companyName: Observable<String?> = Observable(nil)
    var price: Observable<PriceWithKey?> = Observable(nil)
    var percentChange: Observable<String?> = Observable(nil)
    let saveable: Bool
    
    init(stockData: StockData, favoritesService: FavoritesService = UserDefaultsService.instance) {
        symbol.value = stockData.symbol
        companyName.value = stockData.name
        price.value = PriceWithKey(key: stockData.symbol, price: stockData.price)
        percentChange.value = stockData.dayPercentChange
        self.favoritesService = favoritesService
        saveable = !favoritesService.favoritesContain(stockData.symbol)
    }
    
    init(symbol: String, realTimeStockService: RealTimeStockDataRetriever = RealTimeStockService(),
         intradayStockService: IntradayStockDataRetriever = IntradayStockService(),
         timeChecker: CurrentlyTradingDeterminable.Type = TimeChecker.self) {
        saveable = false
        favoritesService = nil

        realTimeStockService.getStockData(for: symbol) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let stockDataResponse):
                guard let stockData = stockDataResponse.first else { return }
                self.symbol.value = stockData.symbol
                self.companyName.value = stockData.name
                self.price.value = PriceWithKey(key: stockData.symbol, price: stockData.price)
                self.percentChange.value = stockData.dayPercentChange
            case .failure(let error):
                print(error.localizedDescription)
            }
        }

        timer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { timer in
            guard timeChecker.isDuringTradingDay() else {
                timer.invalidate()
                return
            }

            intradayStockService.getStockData(for: symbol) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let intradayDataDTO):
                    guard let mostRecentDate = intradayDataDTO.intraday.keys.sorted().last,
                     let price = intradayDataDTO.intraday[mostRecentDate]?.open else { return }
                    self.price.value = PriceWithKey(key: intradayDataDTO.symbol, price: price)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func favoriteStock() {
        guard let symbol = symbol.value else { return }
        favoritesService?.addFavorite(symbol)
    }
}

extension StockCellViewModel: StockCellViewModelUpdateable {
    func stopUpdates() {
        timer?.invalidate()
        timer = nil
    }
}
