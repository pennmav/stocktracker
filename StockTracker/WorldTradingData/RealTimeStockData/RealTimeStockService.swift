//
//  RealTimeStockService.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

protocol RealTimeStockDataRetriever {
    func getStockData(for symbol: String, callback: @escaping (Result<[StockData]>) -> Void)
}

class RealTimeStockService: RealTimeStockDataRetriever {
    private let apiKey = "P7SfgQ4UIhg45ia5985bIjugeQTAtsxsxlpMLE3wad50Kq0valhJyaQdbILG"
    private let requestor: HTTPRequestable
    
    init(requestor: HTTPRequestable = HTTPRequester.instance) {
        self.requestor = requestor
    }
    
    func getStockData(for symbol: String, callback: @escaping (Result<[StockData]>) -> Void) {
        let apiKeyQueryItem = URLQueryItem(name: "api_token", value: apiKey)
        let symbolQueryItem = URLQueryItem(name: "symbol", value: symbol)
        let queryItems = [apiKeyQueryItem,symbolQueryItem]
        requestor.performRequest(scheme: "https", host: "www.worldtradingdata.com", path: "/api/v1/stock", queryItems: queryItems) { result in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    let stockDataResponseDTO = try decoder.decode(StockDataResponseDTO.self, from: data)
                    callback(.success(stockDataResponseDTO.data))
                } catch {
                    callback(.failure(error))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}
