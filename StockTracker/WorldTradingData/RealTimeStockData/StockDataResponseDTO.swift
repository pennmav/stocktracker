//
//  StockDataResponseDTO.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

private enum CodingKeys: String, CodingKey {
    case symbolsRequested = "symbols_requested"
    case symbolsReturned = "symbols_returned"
    case data = "data"
    case symbol = "symbol"
    case name = "name"
    case price = "price"
    case currency = "currency"
    case openPrice = "price_open"
    case dayHigh = "day_high"
    case dayLow = "day_low"
    case fiftyTwoWeekHigh = "52_week_high"
    case fiftyTwoWeekLow = "52_week_low"
    case dayDollarChange = "day_change"
    case dayPercentChange = "change_pct"
    case yesterdaysClose = "close_yesterday"
    case marketCap = "market_cap"
    case volume = "volume"
    case averageVolume = "volume_avg"
    case shares = "shares"
    case stockExchangeLongName = "stock_exchange_long"
    case stockExchangeShortName = "stock_exchange_short"
    case timeZoneAbbreviation = "timezone"
    case timeZoneName = "timezone_name"
    case gmtOffset = "gmt_offset"
    case lastTradeTime = "last_trade_time"
}

struct StockDataResponseDTO {
    let symbolsRequested: Int
    let symbolsReturned: Int
    let data: [StockData]
}

extension StockDataResponseDTO: Codable {
    init(from decoder: Decoder) throws {
        let keyedDecoder = try getKeyedDecoder(from: decoder)
        symbolsRequested = try keyedDecoder.decode(Int.self, forKey: .symbolsRequested)
        symbolsReturned = try keyedDecoder.decode(Int.self, forKey: .symbolsReturned)
        data = try keyedDecoder.decode([StockData].self, forKey: .data)
    }
}

struct StockData {
    let symbol: String
    let name: String
    let price: String
    let currency: String
    let openPrice: String
    let dayHigh: String
    let fiftyTwoWeekHigh: String
    let fiftyTwoWeekLow: String
    let dayDollarChange: String
    let dayPercentChange: String
    let yesterdaysClose: String
    let marketCap: String
    let volume: String
    let averageVolume: String
    let shares: String
    let stockExchangeLongName: String
    let stockExchangeShortName: String
    let timeZoneAbbreviation: String
    let timeZoneName: String
    let gmtOffset: String
    let lastTradeTime: String
}

extension StockData: Codable {
    init(from decoder: Decoder) throws {
        let keyedDecoder = try getKeyedDecoder(from: decoder)
        symbol = try keyedDecoder.decode(String.self, forKey: .symbol)
        name = try keyedDecoder.decode(String.self, forKey: .name)
        price = try keyedDecoder.decode(String.self, forKey: .price)
        currency = try keyedDecoder.decode(String.self, forKey: .currency)
        openPrice = try keyedDecoder.decode(String.self, forKey: .openPrice)
        dayHigh = try keyedDecoder.decode(String.self, forKey: .dayHigh)
        fiftyTwoWeekHigh = try keyedDecoder.decode(String.self, forKey: .fiftyTwoWeekHigh)
        fiftyTwoWeekLow = try keyedDecoder.decode(String.self, forKey: .fiftyTwoWeekLow)
        dayDollarChange = try keyedDecoder.decode(String.self, forKey: .dayDollarChange)
        dayPercentChange = try keyedDecoder.decode(String.self, forKey: .dayPercentChange)
        yesterdaysClose = try keyedDecoder.decode(String.self, forKey: .yesterdaysClose)
        marketCap = try keyedDecoder.decode(String.self, forKey: .marketCap)
        volume = try keyedDecoder.decode(String.self, forKey: .volume)
        averageVolume = try keyedDecoder.decode(String.self, forKey: .averageVolume)
        shares = try keyedDecoder.decode(String.self, forKey: .shares)
        stockExchangeLongName = try keyedDecoder.decode(String.self, forKey: .stockExchangeLongName)
        stockExchangeShortName = try keyedDecoder.decode(String.self, forKey: .stockExchangeShortName)
        timeZoneAbbreviation = try keyedDecoder.decode(String.self, forKey: .timeZoneAbbreviation)
        timeZoneName = try keyedDecoder.decode(String.self, forKey: .timeZoneName)
        gmtOffset = try keyedDecoder.decode(String.self, forKey: .gmtOffset)
        lastTradeTime = try keyedDecoder.decode(String.self, forKey: .lastTradeTime)
    }
}

private func getKeyedDecoder(from decoder: Decoder) throws -> KeyedDecodingContainer<CodingKeys> {
    return try decoder.container(keyedBy: CodingKeys.self)
}
