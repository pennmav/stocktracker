//
//  IntradayStockService.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

protocol IntradayStockDataRetriever {
    func getStockData(for symbol: String, callback: @escaping (Result<IntradayStockResponseDTO>) -> Void)
}

class IntradayStockService: IntradayStockDataRetriever {
    private let apiKey = "P7SfgQ4UIhg45ia5985bIjugeQTAtsxsxlpMLE3wad50Kq0valhJyaQdbILG"
    private let requestor: HTTPRequestable
    
    init(requestor: HTTPRequestable = HTTPRequester.instance) {
        self.requestor = requestor
    }
    
    func getStockData(for symbol: String, callback: @escaping (Result<IntradayStockResponseDTO>) -> Void) {
        let apiKeyQueryItem = URLQueryItem(name: "api_token", value: apiKey)
        let symbolQueryItem = URLQueryItem(name: "symbol", value: symbol)
        let intervalQueryItem = URLQueryItem(name: "interval", value: "1")
        let rangeQueryItem = URLQueryItem(name: "range", value: "1")
        let queryItems = [apiKeyQueryItem,symbolQueryItem, intervalQueryItem, rangeQueryItem]
        requestor.performRequest(scheme: "https", host: "www.worldtradingdata.com", path: "/api/v1/intraday", queryItems: queryItems) { result in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                do {
                    let intradayDTO = try decoder.decode(IntradayStockResponseDTO.self, from: data)
                    callback(.success(intradayDTO))
                } catch {
                    callback(.failure(error))
                }
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}
