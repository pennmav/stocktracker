//
//  IntradayStockResponseDTO.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

private enum CodingKeys: String, CodingKey {
    case symbol
    case timeZoneName = "timezone_name"
    case intraday
}

struct IntradayStockResponseDTO {
    let symbol: String
    let timeZoneName: String
    let intraday: [Date: IntradayStockData]
    
    private static var formatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "America/New_York")
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }
}

extension IntradayStockResponseDTO: Codable {
    init(from decoder: Decoder) throws {
        let keyedDecoder = try getKeyedDecoder(from: decoder)
        symbol = try keyedDecoder.decode(String.self, forKey: .symbol)
        timeZoneName = try keyedDecoder.decode(String.self, forKey: .timeZoneName)
        let intraday = try keyedDecoder.decode([String: IntradayStockData].self, forKey: .intraday)
        self.intraday = intraday.reduce(into: [:]) { result, keyValuePair in
            if let dateKey = IntradayStockResponseDTO.formatter.date(from: keyValuePair.key) {
                result[dateKey] = keyValuePair.value
            }
        }
    }
}

struct IntradayStockData: Codable {
    let open: String
    let close: String
    let high: String
    let low: String
}

private func getKeyedDecoder(from decoder: Decoder) throws -> KeyedDecodingContainer<CodingKeys> {
    return try decoder.container(keyedBy: CodingKeys.self)
}
