//
//  UISearchBarExtension.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/17/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import UIKit.UISearchBar

extension UISearchBar {
    
    var withDone: UISearchBar {
        let doneToolbar = UIToolbar()
        doneToolbar.barStyle = .default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(resignFirstResponder))
        doneToolbar.items = [flexSpace, done]
        doneToolbar.sizeToFit()
        inputAccessoryView = doneToolbar
        return self
    }
}

