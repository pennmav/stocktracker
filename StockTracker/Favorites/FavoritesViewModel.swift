//
//  FavoritesViewModel.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation
import Observable

protocol FavoritesViewModelable {
    var favoritedStockCellViewModels: Observable<[FavoritedStockCellViewModel]> { get }
    func unfavorate(at index: Int)
    func rearrangeFavorites(sourceRow: Int, destinationRow: Int)
}

class FavoritesViewModel: FavoritesViewModelable {
    var favoritedStockCellViewModels: Observable<[FavoritedStockCellViewModel]> = Observable([])
    
    private let favoritesService: FavoritesService
    private var disposal = Disposal()
    
    init(favoritesService: FavoritesService = UserDefaultsService.instance) {
        self.favoritesService = favoritesService
        favoritedStockCellViewModels.value = favoritesService.favorites.value.map { StockCellViewModel(symbol: $0) }
        favoritesService.favorites.observe { [weak self] favorites, _ in
            guard let self = self, self.favoritedStockCellViewModels.value.count < favorites.count, let lastFavorited = favorites.last else { return }
            self.favoritedStockCellViewModels.value = self.favoritedStockCellViewModels.value + [StockCellViewModel(symbol: lastFavorited)]
        }.add(to: &disposal)
    }
    
    func unfavorate(at index: Int) {
        let viewModel = favoritedStockCellViewModels.value.remove(at: index)
        viewModel.stopUpdates()
        favoritesService.unfavorite(at: index)
    }
    
    func rearrangeFavorites(sourceRow: Int, destinationRow: Int) {
        favoritesService.rearrangeFavorites(sourceRow: sourceRow, destinationRow: destinationRow)
        var viewModelsTmp = favoritedStockCellViewModels.value
        let viewModel = viewModelsTmp.remove(at: sourceRow)
        viewModelsTmp.insert(viewModel, at: destinationRow)
        favoritedStockCellViewModels.value = viewModelsTmp
    }
}
