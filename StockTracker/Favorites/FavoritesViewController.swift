//
//  FavoritesViewController.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/16/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import UIKit
import Observable

private enum Section: Int, CaseIterable {
    case favorites = 0
}

class FavoritesViewController: UIViewController {
    private let tableView = UITableView()
    private let stockCellType: StockTableViewCell.Type
    private let viewModel: FavoritesViewModelable
    private var disposal = Disposal()
    
    init(viewModel: FavoritesViewModelable = FavoritesViewModel(), stockCellType: StockTableViewCell.Type = StockCell.self) {
        self.viewModel = viewModel
        self.stockCellType = stockCellType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupTableView()
        bindToViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addEditBarButtonItem()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        endEdit()
        tabBarController?.navigationItem.rightBarButtonItem = nil
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.dataSource = self
        tableView.delegate = self

        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.register(stockCellType, forCellReuseIdentifier: "\(stockCellType)")
    }
    
    private func addEditBarButtonItem() {
        tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(startEdit))
    }
    
    private func bindToViewModel() {
        viewModel.favoritedStockCellViewModels.observe { [weak self] newValue, oldValue in
            guard let self = self else { return }
            if oldValue?.count != newValue.count {
                self.tableView.reloadSections(IndexSet(integer: Section.favorites.rawValue), with: .automatic)
            }
        }.add(to: &disposal)
    }
    
    @objc func startEdit() {
        tableView.setEditing(true, animated: true)
        tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(endEdit))
    }
    
    @objc func endEdit() {
        tableView.setEditing(false, animated: true)
        addEditBarButtonItem()
    }
}

extension FavoritesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section(rawValue: section) else { return 0 }
        switch section {
        case .favorites:
            return viewModel.favoritedStockCellViewModels.value.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section) else { return UITableViewCell() }
        switch section {
        case .favorites:
            guard viewModel.favoritedStockCellViewModels.value.count > indexPath.row else {
                    return UITableViewCell()
            }
            var stockCell = tableView.dequeueReusableCell(withIdentifier: "\(stockCellType)") as! StockTableViewCell
            stockCell.updatesDelegate = tableView
            stockCell.viewModel = viewModel.favoritedStockCellViewModels.value[indexPath.row]
            return stockCell
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.unfavorate(at: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        viewModel.rearrangeFavorites(sourceRow: sourceIndexPath.row, destinationRow: destinationIndexPath.row)
    }
}

extension FavoritesViewController: UITableViewDelegate {
}
