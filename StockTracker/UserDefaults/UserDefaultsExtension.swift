//
//  UserDefaultsExtension.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

protocol UserDefaultsSettable {
    func array(forKey defaultName: String) -> [Any]?
    func set(_ value: Any?, forKey defaultName: String)
}

extension UserDefaults: UserDefaultsSettable {}
