//
//  UserDefaultsService.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation
import Observable

protocol FavoritesService {
    var favorites: Observable<[String]> { get }
    func addFavorite(_ symbol: String)
    func favoritesContain(_ symbol: String) -> Bool
    func unfavorite(at index: Int)
    func rearrangeFavorites(sourceRow: Int, destinationRow: Int)
}

private struct UserDefaultsKeys {
    static let favorites = "favorites"
}

class UserDefaultsService: FavoritesService {
    static let instance = UserDefaultsService()
    
    let favorites: Observable<[String]>

    private let defaults: UserDefaultsSettable
    
    init(defaults: UserDefaultsSettable = UserDefaults.standard) {
        self.defaults = defaults
        favorites = Observable(defaults.array(forKey: UserDefaultsKeys.favorites) as? [String] ?? [])
    }
    
    func addFavorite(_ symbol: String) {
        favorites.value = favorites.value + [symbol]
        defaults.set(favorites.value, forKey: UserDefaultsKeys.favorites)
    }
    
    func favoritesContain(_ symbol: String) -> Bool {
        return favorites.value.contains(symbol)
    }
    
    func unfavorite(at index: Int) {
        favorites.value.remove(at: index)
        defaults.set(favorites.value, forKey: UserDefaultsKeys.favorites)
    }
    
    func rearrangeFavorites(sourceRow: Int, destinationRow: Int) {
        guard sourceRow < favorites.value.count && destinationRow < favorites.value.count else { return }
        var favoritesTmp = favorites.value
        let symbol = favoritesTmp.remove(at: sourceRow)
        favoritesTmp.insert(symbol, at: destinationRow)
        favorites.value = favoritesTmp
        defaults.set(favorites.value, forKey: UserDefaultsKeys.favorites)
    }
}
