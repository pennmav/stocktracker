//
//  UITableViewUpdatesDelegate.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import UIKit

protocol UITableViewUpdatesDelegate: AnyObject {
    func beginUpdates()
    func endUpdates()
}

extension UITableView: UITableViewUpdatesDelegate {}
