//
//  TabBarViewController.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/16/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import UIKit

private enum Tab: Int {
    case favorites = 0, search
}

class TabBarViewController: UITabBarController {
    
    private let favoritesViewController: UIViewController
    private let searchViewController: UIViewController & Searchable

    init(favoritesViewController: UIViewController = FavoritesViewController(),
         searchViewController: UIViewController & Searchable = SearchViewController()) {
        self.favoritesViewController = favoritesViewController
        self.searchViewController = searchViewController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        favoritesViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: Tab.favorites.rawValue)
        searchViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: Tab.search.rawValue)
        
        viewControllers = [favoritesViewController, searchViewController]
        selectedViewController = favoritesViewController
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let tab = Tab(rawValue: selectedIndex) else { return }
        switch tab {
        case .search:
            if let searchViewController = selectedViewController as? Searchable {
                searchViewController.focusSearch()
            }
        default: break
        }
    }
}
