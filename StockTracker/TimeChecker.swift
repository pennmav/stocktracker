//
//  TimeChecker.swift
//  StockTracker
//
//  Created by Matt Pennella on 3/18/19.
//  Copyright © 2019 Matt Pennella. All rights reserved.
//

import Foundation

protocol CurrentlyTradingDeterminable {
    static func isDuringTradingDay(date: Date) -> Bool
}

extension CurrentlyTradingDeterminable {
    static func isDuringTradingDay(date: Date = Date()) -> Bool {
        return isDuringTradingDay(date: date)
    }
}

class TimeChecker: CurrentlyTradingDeterminable {
    static func isDuringTradingDay(date: Date) -> Bool {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(identifier: "America/New_York")!
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let dayOfWeek = calendar.component(.weekday, from: date)
        
        return isWeekDay(dayOfWeek: dayOfWeek) && isPastMarketOpen(hour: hour, minutes: minutes) && isBeforeMarketClose(hour: hour)
    }
    
    private static func isWeekDay(dayOfWeek: Int) -> Bool {
        return dayOfWeek > 1 && dayOfWeek < 7
    }
    
    private static func isPastMarketOpen(hour: Int, minutes: Int) -> Bool {
        return hour > 9 || (hour == 9 && minutes >= 30)
    }
    
    private static func isBeforeMarketClose(hour: Int) -> Bool {
        return hour < 16
    }
}
